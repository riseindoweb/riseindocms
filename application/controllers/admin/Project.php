<?php
// header('Access-Control-Allow-Origin: *');
class Project extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('m_petani');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_general');
		// $this->load->model('m_user');
		$this->load->library('upload');
	}

	function index(){
		if($this->session->userdata('akses')=='1'){
			$x['data']=$this->m_petani->get_all_pengguna();
			$this->load->view('admin/header');
			$this->load->view('admin/v_petani',$x);
			$this->load->view('admin/footer');
		}else{
			redirect('administrator');
		}
	}

	function Listingpetani(){
		if($this->session->userdata('akses')=='1'){
			$x['data']=$this->m_petani->get_all_pengguna();
			$this->load->view('admin/header');
			$this->load->view('admin/v_petani',$x);
			// $this->load->view('admin/footer');
		}else{
			redirect('administrator');
		}
	}
	function Listingprojectpetani(){
		if($this->session->userdata('akses')=='1'){
			$x['data']=$this->m_petani->get_all_project();
			$this->load->view('admin/header');
			$this->load->view('admin/v_project_list',$x);
			// $this->load->view('admin/footer');
		}else{
			redirect('administrator');
		}
	}
	function Add_project_petani(){
		if($this->session->userdata('akses')=='1'){
			$x['petani']=$this->m_petani->get_all_pengguna();
			$x['resiko']=$this->m_general->get_all_risk();
			$x['skema']=$this->m_general->get_all_skema();
			$x['prov']=$this->m_general->provinces_filter();
			$x['kota']=$this->m_general->regencies_filter('91');
			$this->load->view('admin/header');
			$this->load->view('admin/v_project_petani',$x);
			// $this->load->view('admin/footer');
		}else{
			redirect('administrator');
		}
	}
	function save_project() {
		if($this->session->userdata('akses')=='1'){
			// $config['upload_path'] = './uploads/images/'; //path folder

			$namadepan=$this->input->post('xnamadepan');
			$namabelakang=$this->input->post('xnamabelakang');
			$nama = $namadepan.' '.$namabelakang;
			$password=$this->input->post('xpass');
			$confirmpassword=$this->input->post('xconfirmpass');
			$no_ktp=$this->input->post('xktp');
			$email=$this->input->post('xemail');
			$no_telpon=$this->input->post('xtelpon');
			$tgl_lahir=$this->input->post('xttl');
			$alamat=$this->input->post('xalamat');
			$jenis_kelamin=$this->input->post('xjekel');
			$id_provinsi=$this->input->post('xprovinsi');
			$id_regencies=$this->input->post('xkota');
			$kode_post=$this->input->post('xkodepos');
			$id_privilege=$this->input->post('xrole');
			$date =date("Y-m-d H:i:s", strtotime('6 hour'));
        	$config['upload_path'] = './uploads/images/';
	        $config['allowed_types'] = 'jpg|png';
	        $config['max_size']    = '1000';

		    $new_name = $nama.'_'.time();
			$config['file_name'] = $new_name;

		    $this->load->library('upload', $config);
		    $this->upload->initialize($config);
			if ($password !== $confirmpassword) {
				echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> password tidak sama dengan confirmasi password</div>');
				redirect ('admin/Add_petani');
			}
			if ($id_provinsi === '') {
				echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> provinsi harus diisi</div>');
				redirect ('admin/Add_petani');
			}
			if ($id_regencies === '') {
				echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> kota harus diisi</div>');
				redirect ('admin/Add_petani');
			}

	        if (!$this->upload->do_upload('filefoto'))
	        {
	            $upload_error = array('error' => $this->upload->display_errors());
	            $erro_display = $this->upload->display_errors();
	            echo $erro_display;
	        } else {
                $gbr = $this->upload->data();
            	$url_images= './assets/images/'.$gbr['file_name'];
				// $kode_post=$this->input->post('xkodepos');
				$save_user = array(
				'nama'=> $nama, 'password'=> hash('sha256',$password), 'email'=> $email,
				'no_telpon'=> $no_telpon, 'no_ktp'=> $no_ktp, 'tgl_lahir'=> $tgl_lahir,
				'jenis_kelamin'=> $jenis_kelamin,'alamat'=> $alamat, 'id_provinsi'=> $id_provinsi, 
				'id_regencies'=> $id_regencies, 'url_images'=> $url_images, 'kode_post'=> $kode_post);
				$this->m_petani->save_petani($save_user, 'petani');
				// echo ('');
				echo $this->session->set_flashdata('msg','success');
				redirect ('admin/project');
			}
		}else{
			redirect('administrator');
		}
	}
	function Add_petani(){
		if($this->session->userdata('akses')=='1'){
			$this->load->view('admin/header');
			$x['kat']=$this->m_petani->get_all_privilage();
			$x['prov']=$this->m_general->provinces_filter();
			$x['kota']=$this->m_general->regencies_filter('91');
			$this->load->view('admin/v_petani_add',$x);
			// $this->load->view('admin/footer');
		}else{
			redirect('administrator');
		}
	}
	function save() {
		if($this->session->userdata('akses')=='1'){
			// $config['upload_path'] = './uploads/images/'; //path folder

			$namadepan=$this->input->post('xnamadepan');
			$namabelakang=$this->input->post('xnamabelakang');
			$nama = $namadepan.' '.$namabelakang;
			$password=$this->input->post('xpass');
			$confirmpassword=$this->input->post('xconfirmpass');
			$no_ktp=$this->input->post('xktp');
			$email=$this->input->post('xemail');
			$no_telpon=$this->input->post('xtelpon');
			$tgl_lahir=$this->input->post('xttl');
			$alamat=$this->input->post('xalamat');
			$jenis_kelamin=$this->input->post('xjekel');
			$id_provinsi=$this->input->post('xprovinsi');
			$id_regencies=$this->input->post('xkota');
			$kode_post=$this->input->post('xkodepos');
			$id_privilege=$this->input->post('xrole');
			$date =date("Y-m-d H:i:s", strtotime('6 hour'));
        	$config['upload_path'] = './uploads/images/';
	        $config['allowed_types'] = 'jpg|png';
	        $config['max_size']    = '1000';

		    $new_name = $nama.'_'.time();
			$config['file_name'] = $new_name;

		    $this->load->library('upload', $config);
		    $this->upload->initialize($config);
			if ($password !== $confirmpassword) {
				echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> password tidak sama dengan confirmasi password</div>');
				redirect ('admin/Add_petani');
			}
			if ($id_provinsi === '') {
				echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> provinsi harus diisi</div>');
				redirect ('admin/Add_petani');
			}
			if ($id_regencies === '') {
				echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> kota harus diisi</div>');
				redirect ('admin/Add_petani');
			}

	        if (!$this->upload->do_upload('filefoto'))
	        {
	            $upload_error = array('error' => $this->upload->display_errors());
	            $erro_display = $this->upload->display_errors();
	            echo $erro_display;
	        } else {
                $gbr = $this->upload->data();
            	$url_images= './assets/images/'.$gbr['file_name'];
				// $kode_post=$this->input->post('xkodepos');
				$save_user = array(
				'nama'=> $nama, 'password'=> hash('sha256',$password), 'email'=> $email,
				'no_telpon'=> $no_telpon, 'no_ktp'=> $no_ktp, 'tgl_lahir'=> $tgl_lahir,
				'jenis_kelamin'=> $jenis_kelamin,'alamat'=> $alamat, 'id_provinsi'=> $id_provinsi, 
				'id_regencies'=> $id_regencies, 'url_images'=> $url_images, 'kode_post'=> $kode_post);
				$this->m_petani->save_petani($save_user, 'petani');
				// echo ('');
				echo $this->session->set_flashdata('msg','success');
				redirect ('admin/project');
			}
		}else{
			redirect('administrator');
		}
	}
	function Approval(){
		if($this->session->userdata('akses')=='1'){
		// $this->m_kontak->update_status_kontak();
		// $x['data']=$this->m_kontak->get_all_inbox();
			$this->load->view('admin/header');
			$this->load->view('admin/v_dashboard');
			$this->load->view('admin/footer');
		}else{
			redirect('administrator');
		}
	}
	public function regencies_filter_post($id)
    {
    	// echo $id ."iii";
        $cadmin=$this->m_general->regencies_filter($id);
        $dump[] = array('regencies' => $cadmin, 'resultCode' => 'OK');
        echo json_encode($dump);
        // $this->response($cadmin);
    }
}