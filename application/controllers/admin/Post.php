<?php
class Post extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('m_article');
		$this->load->model('m_kategori');
	}
	function listing(){
		if($this->session->userdata('akses')=='1'){
			$x['data']=$this->m_article->get_article_all();
			$this->load->view('admin/header');
			$this->load->view('admin/v_post',$x);
			// $this->load->view('admin/footer');
		}else{
			redirect('administrator');
		}
	}
	function add_article(){
		if($this->session->userdata('akses')=='1'){
		$x['kat']=$this->m_kategori->get_all_kategori('category');
		$this->load->view('admin/header');
		$this->load->view('admin/v_post_add',$x);
		// $this->load->view('admin/footer');
	}else{
			redirect('administrator');
		}
	}
	function category(){
		if($this->session->userdata('akses')=='1'){
		$x['data']=$this->m_kategori->get_all_kategori('category');
		$this->load->view('admin/header');
		$this->load->view('admin/v_kategori',$x);
		// $this->load->view('admin/footer');
	}else{
			redirect('administrator');
		}
	}
	function save_article() {
		if($this->session->userdata('akses')=='1'){
			$config['upload_path'] = './uploads/images/'; 
	        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; 

            $this->load->library('upload', $config);
		    $this->upload->initialize($config);
            $config['create_thumb']= FALSE;
            $config['maintain_ratio']= FALSE;
            $config['quality']= '60%';
            $config['width']= 710;
            $config['height']= 460;
			$title=$this->input->post('xjudul');
			$id_category=$this->input->post('xkategori');
			$description=$this->input->post('xisi');
			$tag=$this->input->post('xtag');
			$author=$this->session->userdata('idadmin');
			$new_name = $title.'_'.time();
			$date =date("Y-m-d H:i:s", strtotime('6 hour'));
			$config['file_name'] = $new_name;
			// $photo = $this->input->post('filefoto');
			if (!$this->upload->do_upload('filefoto'))
	        {
	        	$url_images = '';
	        } else {
                $gbr = $this->upload->data();
            	$url_images= './uploads/images/'.$gbr['file_name'];
			}

			$this->m_article->simpan_tulisan($id_category, $title, $description, $url_images, $tag, $author, $date);
			echo $this->session->set_flashdata('msg','success');
			redirect ('admin/post/listing');
		}else{
			redirect('administrator');
		}
	}
	function simpan_kategori(){
		if($this->session->userdata('akses')=='1'){
			$kategori=strip_tags($this->input->post('xkategori'));
			$this->m_kategori->simpan_kategori($kategori, 'category');
			echo $this->session->set_flashdata('msg','success');
			redirect('admin/post/category');
		}else{
			redirect('administrator');
		}
	}

	function update_kategori(){
		if($this->session->userdata('akses')=='1'){
			$kode=strip_tags($this->input->post('kode'));
			$kategori=strip_tags($this->input->post('xname'));
			$State=strip_tags($this->input->post('xstate'));
			$this->m_kategori->update_kategori($kode,$kategori,$State, 'category');
			echo $this->session->set_flashdata('msg','info');
			redirect('admin/post/category');
		}else{
			redirect('administrator');
		}
	}
	function hapus_kategori(){
		if($this->session->userdata('akses')=='1'){
			$kode=strip_tags($this->input->post('kode'));
			$this->m_kategori->hapus_kategori($kode, 'category');
			echo $this->session->set_flashdata('msg','success-hapus');
			redirect('admin/post/category');
		}else{
			redirect('administrator');
		}
	}
	function potong ($index) {
		return $index;
	}
}