<?php
// header('Access-Control-Allow-Origin: *');
class Login extends CI_Controller{
    function __construct(){
        parent:: __construct();
        $this->load->model('m_login');
    }
    function index(){
        $this->load->view('admin/v_login');
    }
    function auth(){
        // $postdata = file_get_contents('php://input');
        // $request = json_decode($postdata);
        // $username = $request->username;
        // $password = $request->password;
        $username=strip_tags(str_replace("'", "", $this->input->post('username')));
        $password=strip_tags(str_replace("'", "", $this->input->post('password')));
        if($username === ''){
            // print "<script type=\"text/javascript\">alert('masukkan user anda');</script>";
            echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> masukkan user anda</div>');
            redirect ('admin/login');
        }if ($password === '') {
            echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> masukkan password anda</div>');
            redirect ('admin/login');
            // return print "<script type=\"text/javascript\">alert('masukkan password anda');</script>";
            // return alert('masukkan password anda');
        }
        $u=$username;
        $p = hash('sha256', $password);
        // echo $u.$p;
        $cadmin=$this->m_login->cekadmin($u,$p);
        echo json_encode($cadmin);
        if($cadmin->num_rows() > 0){
         $this->session->set_userdata('masuk',true);
         $this->session->set_userdata('user',$u);
         $xcadmin=$cadmin->result();
         $coba = array ();
            foreach ($xcadmin as $row) {
                if($row->pengguna_level=='1'){
                    $this->session->set_userdata('akses','1');
                    $idadmin=$row->pengguna_id;
                    $user_nama=$row->pengguna_nama;
                    $this->session->set_userdata('idadmin',$idadmin);
                    $this->session->set_userdata('nama',$user_nama);
                    redirect('admin/dashboard');
                }else{
                     $this->session->set_userdata('akses','2');
                     $idadmin=$row->pengguna_id;
                     $user_nama=$row->pengguna_nama;
                     $this->session->set_userdata('idadmin',$idadmin);
                     $this->session->set_userdata('nama',$user_nama);
                     redirect('admin/dashboard');
                }
            }

        }else{
         echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Username Atau Password Salah</div>');
         redirect('admin/login');
        }

    }

    function logout(){
        $this->session->sess_destroy();
        redirect('admin/login');
    }
}
