<div class="content-wrapper">
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
          
          <div class="box">
            <section class="content-header">
            <h1>
              Petani
              <small></small>
            </h1>
            <ol class="breadcrumb">
              <li><a href="<?php echo base_url().'admin/dashboard/'?>"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Petani</li>
            </ol>
          </section>
            <div class="box-header">
              <a class="btn btn-success btn-flat" href="<?php echo base_url().'admin/project/add_petani'?>"><span class="fa fa-plus"></span> Add Petani</a>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-striped" style="font-size:13px;">
                <thead>
                <tr>
                  <th>id</th>
                  <th>nama</th>
                  <th>email</th>
                  <th>no telp.</th>
                  <th>alamat</th>
                  <th>status</th>
                  <th style="text-align:right;">Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    $no=0;
                    foreach ($data->result_array() as $i) :
                      $no++;
                      $id=$i['petani_id'];
                      $nama=$i['nama'];
                      $email=$i['email'];
                      $no_telpon=$i['no_telpon'];
                      $alamat=$i['alamat'];
                      $state=$i['status'];
                    ?>
                <tr>
                  <td><?php echo $id;?></td>
                  <td><?php echo $nama;?></td>
                  <td><?php echo $email;?></td>
                  <td><?php echo $no_telpon;?></td>
                  <td><?php echo $alamat;?></td>
                  <?php if($state=='1'):?>
                  <td>Active</td>
                  <?php else:?>
                  <td>Not Active</td>
                  <?php endif;?>
                  <td style="text-align:right;">
                    <a class="btn" data-toggle="modal" data-target="#ModalEdit<?php echo $id;?>"><span class="fa fa-pencil"></span></a>
                    <a class="btn" data-toggle="modal" data-target="#ModalHapus<?php echo $id;?>"><span class="fa fa-trash"></span></a>
                  </td>
                </tr>
                <?php endforeach;?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
   </div>
    <?php foreach ($data->result_array() as $i) :
      $id=$i['petani_id'];
      $nama=$i['nama'];
      $email=$i['email'];
      $no_telpon=$i['no_telpon'];
      $alamat=$i['alamat'];
      $state=$i['status'];
    ?>
    <div class="modal fade" id="ModalEdit<?php echo $id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel">Edit Guru</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url().'admin/post/update_kategori'?>" method="post" enctype="multipart/form-data">
              <div class="modal-body">
                <input type="hidden" name="kode" value="<?php echo $id;?>"/>
                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Nama</label>
                  <div class="col-sm-7">
                    <input type="text" name="name" value="<?php echo $nama;?>" class="form-control" id="inputUserName" placeholder="NIP" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Status</label>
                  <div class="col-sm-7">
                    <?php if($state=='1'):?>
                     <div class="radio radio-info radio-inline">
                        <input type="radio" id="inlineRadio1" value="1" name="xstate" checked>
                        <label for="inlineRadio1"> Active</label>
                      </div>
                      <div class="radio radio-info radio-inline">
                        <input type="radio" id="inlineRadio1" value="2" name="xstate">
                        <label for="inlineRadio2"> Not Active </label>
                      </div>
                    <?php else:?>
                      <div class="radio radio-info radio-inline">
                        <input type="radio" id="inlineRadio1" value="1" name="xstate">
                        <label for="inlineRadio1"> Active</label>
                      </div>
                      <div class="radio radio-info radio-inline">
                        <input type="radio" id="inlineRadio1" value="2" name="xstate" checked>
                        <label for="inlineRadio2"> Not Active </label>
                      </div>
                    <?php endif;?>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
              </div>
            </form>
          </div>
        </div>
    </div>
    <?php endforeach;?>
  <!--Modal Edit Album-->

  <?php foreach ($data->result_array() as $i) :
      $id=$i['petani_id'];
      $nama=$i['nama'];
      $email=$i['email'];
      $no_telpon=$i['no_telpon'];
      $alamat=$i['alamat'];
      $state=$i['status'];
    ?>
    <div class="modal fade" id="ModalHapus<?php echo $id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
            <h4 class="modal-title" id="myModalLabel">Hapus Kategori</h4>
          </div>
          <form class="form-horizontal" action="<?php echo base_url().'admin/post/hapus_kategori'?>" method="post" enctype="multipart/form-data">
            <div class="modal-body">
              <input type="hidden" name="kode" value="<?php echo $id;?>"/>
              <p>Apakah Anda yakin mau menghapus kategori <b><?php echo $nama;?></b> ?</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary btn-flat" id="simpan">Hapus</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  <?php endforeach;?>
  <footer class="main-footer">
    </footer>
</div>
<script src="<?php echo base_url().'assets/plugins/jQuery/jquery-2.2.3.min.js'?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url().'assets/bootstrap/js/bootstrap.min.js'?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url().'assets/plugins/datatables/jquery.dataTables.min.js'?>"></script>
<script src="<?php echo base_url().'assets/plugins/datatables/dataTables.bootstrap.min.js'?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url().'assets/plugins/slimScroll/jquery.slimscroll.min.js'?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url().'assets/plugins/fastclick/fastclick.js'?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url().'assets/dist/js/app.min.js'?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url().'assets/dist/js/demo.js'?>"></script>
<script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
  $(function () {
    // CKEDITOR.replace('ckeditor');
  });
</script>
</body>
</html>
  