<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>RiseIndo | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="shorcut icon" type="text/css" href="<?php echo base_url().'assets/images/favicon.png'?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/bootstrap/css/bootstrap.min.css'?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/font-awesome/css/font-awesome.min.css'?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/dist/css/AdminLTE.min.css'?>">
  <link href="<?php echo base_url().'assets/style/signin.css'?>" rel="stylesheet"/>


</head>
<body class="hold-transition login-page">
  <div style="margin-top:-40px !important;">
   <?php echo $this->session->flashdata('msg');?>
  </div>
<div class="container">
     <form action="<?php echo site_url().'admin/login/auth'; ?>" method="post">
   <div class="form-signin">
    <h2 class="form-signin-heading">Please sign in</h2>
     <label for="iduser">Username:</label>
     <input type="text" class="ib-form-control" id="iduser" name="username" placeholder="username"/>
     

     <label for="passuser">Password:</label>
     <input type="password" class="ib-form-control" id="passuser" name="password" placeholder="password"/>
     <?php echo validation_errors(); ?>
     <button class="btn btn-lg btn-primary btn-block" type="submit" value="Login">Sign in</button>
   </div>
   </form>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url().'assets/plugins/jQuery/jquery-2.2.3.min.js'?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url().'assets/bootstrap/js/bootstrap.min.js'?>"></script>
<!-- iCheck -->
</body>
</html>
