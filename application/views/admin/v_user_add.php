<div class="content-wrapper">
    <section class="content-header">
      <h1>
        User
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">User</a></li>
        <li class="active">Add User</li>
      </ol>
    </section>
    <section class="content">
      <!-- <div class="box box-default"> -->
        <!-- <div class="box-header with-border">
          <h3 class="box-title">Post Artikel</h3>
        </div> -->
    <div style="margin-top:-40px !important;">
     <?php echo $this->session->flashdata('msg');?>
    </div>
    <?= form_open_multipart('admin/user/save');?>
      <!-- <div class="box-body">
        <div class="row">
          <div class="col-md-10">
            <input type="text" name="xjudul" class="form-control" placeholder="Judul berita atau artikel" required/>
          </div>
          <div class="col-md-2">
          </div>
        </div>
      </div> -->
    <!-- </div> -->
      <div class="row">
        <div class="col-md-8">
          <div class="box box-danger col-md-12" style="padding:20px 0;">
            <div class="box-body col-md-6">
              <input type="text" name="xnamadepan" class="form-control" placeholder="Nama Depan" required/>
            </div>
            <div class="box-body col-md-6">
              <input type="text" name="xnamabelakang" class="form-control" placeholder="Nama Belakang" required/>
            </div>
            <div class="box-body col-md-6">
              <input type="Password" name="xpass" class="form-control" placeholder="Password" required/>
            </div>
            <div class="box-body col-md-6">
              <input type="Password" name="xconfirmpass" class="form-control" placeholder="Konfirmasi Password" required/>
            </div>
            <div class="box-body col-md-6">
              <input type="number" name="xktp" class="form-control" placeholder="Nomor Ktp / Sim" required/>
            </div>
            <div class="box-body col-md-6">
              <input type="number" name="xtelpon" class="form-control" placeholder="Nomor Telpon" required/>
            </div>
            <div class="box-body col-md-6">
              <input type="email" name="xemail" class="form-control" placeholder="Email" required/>
            </div>
            <div class="box-body col-md-6">
              <input type="date" name="xttl" class="form-control" placeholder="Tanggal Lahir" required/>
            </div>
            <div class="box-body col-md-12">
              <textarea type="text" name="xalamat" class="form-control" placeholder="Alamat Lengkap" required  style="height:200px"/></textarea> 
            </div>
            <div class="box-body col-md-6">
              <select name="xjekel" class="form-control" required>
                <option value="">-Pilih Jenis Kelamin-</option>
                <option value="laki-laki"> Laki - Laki </option>
                <option value="perempuan"> Perempuan </option>
              </select>
            </div>
            <div class="box-body col-md-6">
              <select class="form-control select2" name="xprovinsi" style="width: 100%;" required>
                <option value="">-Pilih Provinsi-</option>
                <?php
                  $no=0;
                  foreach ($prov as $i) :
                     $no++;
                     $id=$i['id'];
                     $nama=$i['name'];
                  ?>
                  <option value="<?php echo $id;?>"><?php echo $nama;?></option>
                <?php endforeach;?>
              </select>
            </div>
            <div class="box-body col-md-6">
              <select class="form-control select2" name="xkota" style="width: 100%;" required>
              </select>
            </div>
            <div class="box-body col-md-6">
              <input type="text" name="xkodepos" class="form-control" placeholder="Kodepos" required/>
            </div>
           </div>
        </div>
        <div class="col-md-4">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Pengaturan Lainnya</h3>
            </div>
            <div class="box-body">
              <div class="form-group">
                <label>User Group</label>
                <select class="form-control select2" name="xrole" style="width: 100%;" required>
                  <option value="">-Pilih-</option>
                  <?php
                    $no=0;
                    foreach ($kat->result_array() as $i) :
                       $no++;
                       $id_privilege=$i['id_privilege'];
                       $nama=$i['nama'];
                    ?>
                    <option value="<?php echo $id_privilege;?>"><?php echo $nama;?></option>
                  <?php endforeach;?>
                </select>
              </div>
                <label>Gambar</label>
                <input type="file" name="filefoto" style="width: 100%;" required>
              <div class="form-group" style="margin-top:20px;">
                <button type="submit" class="btn btn-primary btn-flat pull-right"><span class="fa fa-pencil"></span> Publish</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
  </div>
  <footer class="main-footer">
    </footer>
</div>
<script src="<?php echo base_url().'assets/plugins/jQuery/jquery-2.2.3.min.js'?>"></script>

<script src="<?php echo base_url().'assets/bootstrap/js/bootstrap.min.js'?>"></script>
<script src="<?php echo base_url().'assets/plugins/datatables/jquery.dataTables.min.js'?>"></script>
<script src="<?php echo base_url().'assets/plugins/datatables/dataTables.bootstrap.min.js'?>"></script>

<script src="<?php echo base_url().'assets/plugins/slimScroll/jquery.slimscroll.min.js'?>"></script>

<script src="<?php echo base_url().'assets/plugins/fastclick/fastclick.js'?>"></script>

<script src="<?php echo base_url().'assets/dist/js/app.min.js'?>"></script>

<script src="<?php echo base_url().'assets/dist/js/demo.js'?>"></script>
<script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $('select[name="xprovinsi"]').on('change', function() {
      var stateID = $(this).val();
      if(stateID) {
        $.ajax({
          url: 'regencies_filter_post/'+stateID,
          type: "GET",
          dataType: 'json',
          success:function(data) {
            $('select[name="xkota"]').empty();
            $.each(data[0].regencies, function(key, value) {
              $('select[name="xkota"]').append('<option value="'+ value.id +'">'+ value.name +'</option>');
            });
          }
        });
      }else{
        $('select[name="xkota"]').empty();
      }
    });
  });
</script>
</body>
</html>
