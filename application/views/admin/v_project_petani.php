<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Petani
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Project</a></li>
        <li class="active">Add Petani</li>
      </ol>
    </section>
    <section class="content">
    <div style="margin-top:-40px !important;">
     <?php echo $this->session->flashdata('msg');?>
    </div>
    <?= form_open_multipart('admin/project/save');?>
      <div class="row">
        <div class="col-md-8">
          <div class="box box-danger col-md-12" style="padding:20px 0;">
            <div class="box-body col-md-6">
              <select class="form-control select2" name="xpetani" style="width: 100%;" required>
                <option value="">-Pilih Petani-</option>
                <?php
                  $no=0;
                  foreach ($petani->result_array()  as $i) :
                     $no++;
                     $id=$i['petani_id'];
                     $nama_petani=$i['nama'];
                  ?>
                  <option value="<?php echo $id;?>"><?php echo $nama_petani;?></option>
                <?php endforeach;?>
              </select>
            </div>
            <div class="box-body col-md-6">
              <select class="form-control select2" name="xresiko" style="width: 100%;" required>
                <option value="">-Pilih Resiko-</option>
                <?php
                  $no=0;
                  foreach ($resiko as $i) :
                     $no++;
                     $id=$i['id_category_resiko'];
                     $nama=$i['nama'];
                  ?>
                  <option value="<?php echo $id;?>"><?php echo $nama;?></option>
                <?php endforeach;?>
              </select>
            </div>
            <div class="box-body col-md-6">
              <input type="date" name="xstartfrom" class="form-control" placeholder="start From" required/>
            </div>
            <div class="box-body col-md-6">
              <input type="number" name="xprofit" class="form-control" placeholder="profit" step="0.01" min="0"required/>
            </div>
            <div class="box-body col-md-6">
              <select class="form-control select2" name="xskema" style="width: 100%;" required>
                <option value="">-Pilih Skema-</option>
                <?php
                  $no=0;
                  foreach ($skema as $i) :
                     $no++;
                     $id=$i['id_category_resiko'];
                     $nama=$i['nama'];
                  ?>
                  <option value="<?php echo $id;?>"><?php echo $nama;?></option>
                <?php endforeach;?>
              </select>
            </div>
            <div class="box-body col-md-6">
              <input type="text" name="xlamaproyek" class="form-control" placeholder="Lama Proyek" required/>
            </div>
            <div class="box-body col-md-6">
              <input type="text" name="xkebutuhan" class="form-control" placeholder="Kebutuhan Dana" required/>
            </div>
            <div class="box-body col-md-12">
              <textarea type="text" name="xalamat" class="form-control" placeholder="Alamat Lengkap" required  style="height:200px"/></textarea> 
            </div>
            <div class="box-body col-md-6">
              <select class="form-control select2" name="xprovinsi" style="width: 100%;" required>
                <option value="">-Pilih Provinsi-</option>
                <?php
                  $no=0;
                  foreach ($prov as $i) :
                     $no++;
                     $id=$i['id'];
                     $nama=$i['name'];
                  ?>
                  <option value="<?php echo $id;?>"><?php echo $nama;?></option>
                <?php endforeach;?>
              </select>
            </div>
            <div class="box-body col-md-6">
              <select class="form-control select2" name="xkota" style="width: 100%;" required>
              </select>
            </div>
            <div class="box-body col-md-6">
              <input type="text" name="xkodepos" class="form-control" placeholder="Kodepos" required/>
            </div>
           </div>
        </div>
        <div class="col-md-4">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Pengaturan Lainnya</h3>
            </div>
            <div class="box-body">
              <div class="form-group">
                <label>Gambar</label>
                <input type="file" name="filefoto" style="width: 100%;" required>
              <div class="form-group" style="margin-top:20px;">
                <button type="submit" class="btn btn-primary btn-flat pull-right"><span class="fa fa-pencil"></span> Publish</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
  </div>
  <footer class="main-footer">
    </footer>
</div>
<script src="<?php echo base_url().'assets/plugins/jQuery/jquery-2.2.3.min.js'?>"></script>

<script src="<?php echo base_url().'assets/bootstrap/js/bootstrap.min.js'?>"></script>
<script src="<?php echo base_url().'assets/plugins/datatables/jquery.dataTables.min.js'?>"></script>
<script src="<?php echo base_url().'assets/plugins/datatables/dataTables.bootstrap.min.js'?>"></script>

<script src="<?php echo base_url().'assets/plugins/slimScroll/jquery.slimscroll.min.js'?>"></script>

<script src="<?php echo base_url().'assets/plugins/fastclick/fastclick.js'?>"></script>

<script src="<?php echo base_url().'assets/dist/js/app.min.js'?>"></script>

<script src="<?php echo base_url().'assets/dist/js/demo.js'?>"></script>
<script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $('select[name="xprovinsi"]').on('change', function() {
      var stateID = $(this).val();
      if(stateID) {
        $.ajax({
          url: 'regencies_filter_post/'+stateID,
          type: "GET",
          dataType: 'json',
          success:function(data) {
            $('select[name="xkota"]').empty();
            $.each(data[0].regencies, function(key, value) {
              $('select[name="xkota"]').append('<option value="'+ value.id +'">'+ value.name +'</option>');
            });
          }
        });
      }else{
        $('select[name="xkota"]').empty();
      }
    });
  });
</script>
</body>
</html>
