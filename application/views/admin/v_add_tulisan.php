<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Artikel
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url().'admin/dashboard/'?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url().'admin/post/listing'?>">Artikel</a></li>
        <li class="active">Add Artikel</li>
      </ol>
    </section>
    <section class="content">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Post Artikel</h3>
        </div>
		<form action="<?php echo base_url().'admin/post/save_article'?>" method="post" enctype="multipart/form-data">
      <div class="box-body">
        <div class="row">
          <div class="col-md-10">
            <input type="text" name="xjudul" class="form-control" placeholder="Judul berita atau artikel" required/>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-flat pull-right"><span class="fa fa-pencil"></span> Publish</button>
            </div>
          </div>
        </div>
      </div>
	  </div>
      <div class="row">
        <div class="col-md-8">
          <div class="box box-danger">
            <div class="box-header">
              <h3 class="box-title">Artikel</h3>
            </div>
            <div class="box-body">
              <textarea id="ckeditor" name="xisi" required></textarea>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Pengaturan Lainnya</h3>
            </div>
            <div class="box-body">
              <div class="form-group">
                <label>Kategori</label>
                <select class="form-control select2" name="xkategori" style="width: 100%;" required>
                  <option value="">-Pilih-</option>
        				  <?php
          					$no=0;
          					foreach ($kat->result_array() as $i) :
          					   $no++;
                       $kategori_id=$i['id_category'];
                       $kategori_nama=$i['nama'];
                    ?>
                    <option value="<?php echo $kategori_id;?>"><?php echo $kategori_nama;?></option>
        				  <?php endforeach;?>
                </select>
              </div>
            </div>
		      </form>
        </div>
      </div>
    </section>
  </div>
  <footer class="main-footer">
    </footer>
</div>
<script src="<?php echo base_url().'assets/plugins/jQuery/jquery-2.2.3.min.js'?>"></script>

<script src="<?php echo base_url().'assets/bootstrap/js/bootstrap.min.js'?>"></script>
<script src="<?php echo base_url().'assets/plugins/datatables/jquery.dataTables.min.js'?>"></script>
<script src="<?php echo base_url().'assets/plugins/datatables/dataTables.bootstrap.min.js'?>"></script>

<script src="<?php echo base_url().'assets/plugins/slimScroll/jquery.slimscroll.min.js'?>"></script>

<script src="<?php echo base_url().'assets/plugins/fastclick/fastclick.js'?>"></script>

<script src="<?php echo base_url().'assets/dist/js/app.min.js'?>"></script>

<script src="<?php echo base_url().'assets/dist/js/demo.js'?>"></script>
<script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>

<script>
  $(function () {
    CKEDITOR.replace('ckeditor');
  });
</script>
</body>
</html>
