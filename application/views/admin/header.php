
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Riseindo | Admin</title>
  <link rel="shorcut icon" type="text/css" href="<?php echo base_url().'assets/images/favicon.png'?>">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url().'assets/bootstrap/css/bootstrap.min.css'?>">
  <link rel="stylesheet" href="<?php echo base_url().'assets/font-awesome/css/font-awesome.min.css'?>">
  <link rel="stylesheet" href="<?php echo base_url().'assets/dist/css/AdminLTE.min.css'?>">
  <link rel="stylesheet" href="<?php echo base_url().'assets/dist/css/skins/_all-skins.min.css'?>">
  <link rel="stylesheet" href="<?php echo base_url().'assets/dist/css/mine.css'?>">
  <style>
    a {
      color: black;
    }
    /*.content {
      height: 600px !important;
    }*/
    .c-red{
      color:red;
    }
    .dataTables_paginate {
      text-align: right;
      margin-top: -15px;
    }
    .dataTables_filter {
      text-align: right;
      /*margin-top: -15px;*/
    }
    .dataTables_filter .form-control {
      margin-left:15px;
    }
    .dataTables_length .form-control {
      margin-left :10px;
      margin-right :10px;
    }
  </style>
</head>
<body class="hold-transition skin-red sidebar-mini" >
<div class="wrapper">
  <?php
    $this->load->view('admin/v_header');
  ?>
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="header">Menu Utama</li>
        <li <?php if($this->uri->segment(2)=="dashboard"){echo 'class="active"';} ?>>
          <a href="<?php echo base_url().'admin/dashboard'?>">
            <i class="fa fa-home"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        <li <?php if($this->uri->segment(2)=="user"){echo 'class="active treeview"';} ?>>
          <a href="#">
            <i class="fa fa-user"></i> <span>User</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($this->uri->segment(3)=="listing"){echo 'class="active"';} ?>>
              <a href="<?php echo base_url().'admin/user/listing'?>">
                <i class="fa fa-list"></i> List User
              </a>
            </li>
            <li <?php if($this->uri->segment(3)=="add_user"){echo 'class="active"';} ?>>
              <a href="<?php echo base_url().'admin/user/add_user'?>">
                <i class="fa fa-user-plus"></i> Add User
              </a>
            </li>
            <!-- <li <?php if($this->uri->segment(3)=="listingpetani"){echo 'class="active"';} ?>>
              <a href="<?php echo base_url().'admin/user/listingpetani'?>">
                <i class="fa fa-list"></i> List Petani
              </a>
            </li>
            <li <?php if($this->uri->segment(3)=="add_petani"){echo 'class="active"';} ?>>
              <a href="<?php echo base_url().'admin/user/add_petani'?>">
                <i class="fa fa-user-plus"></i> Add Petani
              </a>
            </li> -->
            <!-- <li <?php if($this->uri->segment(3)=="approval"){echo 'class="active"';} ?>>
              <a href="<?php echo base_url().'admin/user/approval'?>">
                <i class="fa fa-clipboard"></i> Approval User
              </a>
            </li> -->
          </ul>
        </li>
        <li <?php if($this->uri->segment(2)=="project"){echo 'class="active treeview"';} ?>>
          <a href="#">
            <i class="fa fa-bank"></i> <span>Project</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($this->uri->segment(3)=="listingpetani"){echo 'class="active"';} ?>>
              <a href="<?php echo base_url().'admin/project/listingpetani'?>">
                <i class="fa fa-list"></i> List Petani
              </a>
            </li>
            <li <?php if($this->uri->segment(3)=="add_petani"){echo 'class="active"';} ?>>
              <a href="<?php echo base_url().'admin/project/add_petani'?>">
                <i class="fa fa-user-secret"></i> Add Petani
              </a>
            </li>
            <li <?php if($this->uri->segment(3)=="Listingprojectpetani"){echo 'class="active"';} ?>>
              <a href="<?php echo base_url().'admin/project/Listingprojectpetani'?>">
                <i class="fa fa-paypal"></i> Project Petani
              </a>
            </li>
            <li <?php if($this->uri->segment(3)=="add_project_petani"){echo 'class="active"';} ?>>
              <a href="<?php echo base_url().'admin/project/add_project_petani'?>">
                <i class="fa fa-bitbucket-square"></i> Add Project Petani
              </a>
            </li>
          </ul>
        </li>
        <li <?php if($this->uri->segment(2)=="post"){echo 'class="active treeview"';} ?>>
          <a href="#">
            <i class="fa fa-newspaper-o"></i> <span>Artikel</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($this->uri->segment(3)=="listing"){echo 'class="active"';} ?>>
              <a href="<?php echo base_url().'admin/post/listing'?>">
                <i class="fa fa-list"></i> List Artikel
              </a>
            </li>
            <li <?php if($this->uri->segment(3)=="add_article"){echo 'class="active"';} ?>>
              <a href="<?php echo base_url().'admin/post/add_article'?>">
                <i class="fa fa-thumb-tack"></i> Add Artikel
              </a>
            </li>
            <li <?php if($this->uri->segment(3)=="category"){echo 'class="active"';} ?>>
              <a href="<?php echo base_url().'admin/post/category'?>">
                <i class="fa fa-wrench"></i>kategori
              </a>
            </li>
            <!-- <li <?php if($this->uri->segment(3)=="image_slider_add"){echo 'class="active"';} ?>>
              <a href="<?php echo base_url().'admin/post/image_slider_add'?>">
                <i class="fa fa-camera"></i>Post Image Slider
              </a>
            </li>
            <li <?php if($this->uri->segment(3)=="image_slider"){echo 'class="active"';} ?>>
              <a href="<?php echo base_url().'admin/post/image_slider'?>">
                <i class="fa fa-photo"></i>List Image Slider
              </a>
            </li> -->
          </ul>
        </li>
        <li <?php if($this->uri->segment(2)=="report"){echo 'class="active treeview"';} ?>>
          <a href="#">
            <i class="fa fa-file-archive-o"></i> <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <!-- <li <?php if($this->uri->segment(3)=="petani"){echo 'class="active"';} ?>>
              <a href="<?php echo base_url().'admin/report/petani'?>">
                <i class="fa fa-money"></i> Project Petani
              </a>
            </li> -->
            <li <?php if($this->uri->segment(3)=="investor"){echo 'class="active"';} ?>>
              <a href="<?php echo base_url().'admin/report/investor'?>">
                <i class="fa fa-google-wallet"></i> Pemberi Modal
              </a>
            </li>
            <li <?php if($this->uri->segment(3)=="approval"){echo 'class="active"';} ?>>
              <a href="<?php echo base_url().'admin/report/approval'?>">
                <i class="fa fa-balance-scale"></i>List Approval Investor
              </a>
            </li>
          </ul>
        </li>
        <li <?php if($this->uri->segment(2)=="setting"){echo 'class="active treeview"';} ?>>
          <a href="#">
            <i class="fa fa-gear"></i> <span>Setting</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($this->uri->segment(3)=="category_resiko"){echo 'class="active"';} ?>>
              <a href="<?php echo base_url().'admin/setting/category_resiko'?>">
                <i class="fa fa-wrench"></i> Categori Resiko
              </a>
            </li>
            <li <?php if($this->uri->segment(3)=="skema"){echo 'class="active"';} ?>>
              <a href="<?php echo base_url().'admin/setting/skema'?>">
                <i class="fa fa-recycle"></i> Skema
              </a>
            </li>
            <li <?php if($this->uri->segment(3)=="sosmed"){echo 'class="active"';} ?>>
              <a href="<?php echo base_url().'admin/setting/sosmed'?>">
                <i class="fa fa-facebook"></i>sosmed
              </a>
            </li>
          </ul>
        </li>
         <li>
          <a href="<?php echo base_url().'admin/login/logout'?>">
            <i class="fa fa-sign-out"></i> <span>Sign Out</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>


      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>