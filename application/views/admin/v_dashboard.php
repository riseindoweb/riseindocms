  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Dashboard
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-chrome"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Chrome</span>
              <span class="info-box-number">1515</span>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-firefox"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Mozilla Firefox</span>
              <span class="info-box-number">125</span>
            </div>
          </div>
        </div>
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-bug"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Googlebot</span>
              <span class="info-box-number">14</span>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-opera"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Opera</span>
              <span class="info-box-number">91</span>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Pengunjung bulan ini</h3>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-12">
                      <canvas id="canvas" width="1000" height="280"></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-8">
          <!-- MAP & BOX PANE --> 
          <div class="content"> 
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12 n-p-l-r">
                <div class="col-md-4 col-sm-6 col-xs-12 n-p-l">
                  <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-opera"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Opera</span>
                      <span class="info-box-number">91</span>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 n-p-l-r">
                  <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-opera"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Opera</span>
                      <span class="info-box-number">91</span>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 n-p-r">
                  <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-opera"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Opera</span>
                      <span class="info-box-number">91</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 n-p-l-r">
                <div class="col-md-4 col-sm-6 col-xs-12 n-p-l">
                  <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-opera"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Opera</span>
                      <span class="info-box-number">91</span>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 n-p-l-r">
                  <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-opera"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Opera</span>
                      <span class="info-box-number">91</span>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 n-p-r">
                  <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-opera"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Opera</span>
                      <span class="info-box-number">91</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Posting Populer</h3>
              <table class="table">
                <tbody>
                  <tr>
                    <td>Proses belajar mengajar MSchool</td>
                    <td>162 Views</td>
                  </tr>
                  <tr>
                    <td>Persiapan siswa menjelang ujian nasional</td>
                    <td>27 Views</td>
                  </tr>
                  <tr>
                    <td>Pelaksanaan Ujian Nasional MSchool</td>
                    <td>3 Views</td>
                  </tr>
                  <tr>
                    <td>iPhone 8 Baru Mengungkapkan Fitur Mengecewakan</td>
                    <td>3 Views</td>
                  </tr>
                  <tr>
                    <td>Prestasi membangga dari siswa MSchool</td>
                    <td>1 Views</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-safari"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Safari</span>
              <span class="info-box-number">12</span>

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
                  <span class="progress-description">
                    Penggunjung
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-globe"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Lainnya</span>
              <span class="info-box-number">899</span>

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
                  <span class="progress-description">
                    Pengunjung
                  </span>
            </div>
          </div>
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="fa fa-users"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Pengunjung Bulan Lalu</span>
              <span class="info-box-number">89900</span>

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
                  <span class="progress-description">
                    Pengunjung
                  </span>
            </div>
          </div>
          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fa fa-users"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Pengunjung Bulan ini</span>
              <span class="info-box-number">89900</span>

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
                  <span class="progress-description">
                    Pengunjung
                  </span>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

 
