<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class RandomText {
    // function __construct()
    // {
    //     // parent::__construct();
    // }
    function generate($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    function generaterandomstring($length) {
        // $length = 10;
	    $obj1 = $this->generate($length);  
	    $obj2 = $this->generate($length);  
	    $obj3 = $this->generate($length);  
	    $obj4 = $this->generate($length);  
	    $obj5 = $this->generate($length);  
	    $randomString2 = $obj1 .'-'. $obj2 .'-'. $obj3 .'-'. $obj4 .'-'. $obj5;
	    return $randomString2;
	}
}