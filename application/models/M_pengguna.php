<?php
class M_pengguna extends CI_Model{

	function get_all_pengguna(){
		$hsl=$this->db->query("SELECT `user_id`, `nama`, `email`, `no_telpon`, `status` FROM user where status != '99'");
		return $hsl;	
	}
	function get_all_privilage () {
		$hsl=$this->db->query("SELECT * FROM privilage ");
		return $hsl;
	}
	// function simpan_user($nama,$jenkel,$username,$password,$email,$nohp,$level,$gambar){
	// 	$hsl=$this->db->query("INSERT INTO user (nama, password, email, no_telpon, no_ktp, tgl_lahir, alamat, jenis_kelamin, id_provinsi, id_regencies, url_images, kode_post) VALUES ('$nama', '$password', '$email', '$no_telpon', '$no_ktp', '$tgl_lahir', '$alamat', '$jenis_kelamin', '$id_provinsi', '$id_regencies', '$url_images', '$kode_post'");
	// 	return $hsl;
	// }
	function simpan_user($nama,$password,$email,$no_telpon,$no_ktp,$tgl_lahir,$alamat,$jenis_kelamin,$id_provinsi,$id_regencies,$url_images,$kode_post){
		$hsl=$this->db->query("INSERT INTO user (nama, password, email, no_telpon, no_ktp, tgl_lahir, alamat, jenis_kelamin, id_provinsi, id_regencies, url_images, kode_post) VALUES ('$nama', '$password', '$email', '$no_telpon', '$no_ktp', '$tgl_lahir', '$alamat', '$jenis_kelamin', '$id_provinsi', '$id_regencies', '$url_images', '$kode_post'");
		return $hsl;
	}
	function save_user($person, $table_name, $privilage, $date){
	$this->db->insert($table_name,$person);
	$id = $this->db->insert_id();
	//menambahkan nama yang tidak sama
		$hsl=$this->db->query("INSERT INTO `role_membership`( `id_privilege`, `user_id`, `date`) VALUES ('$privilage','$id','$date')");
		return $hsl;
	}
	function simpan_pengguna_tanpa_gambar($nama,$jenkel,$username,$password,$email,$nohp,$level){
		$hsl=$this->db->query("INSERT INTO tbl_pengguna (pengguna_nama,pengguna_jenkel,pengguna_username,pengguna_password,pengguna_email,pengguna_nohp,pengguna_level) VALUES ('$nama','$jenkel','$username',md5('$password'),'$email','$nohp','$level')");
		return $hsl;
	}

	//UPDATE PENGGUNA //
	function update_pengguna_tanpa_pass($kode,$nama,$jenkel,$username,$password,$email,$nohp,$level,$gambar){
		$hsl=$this->db->query("UPDATE tbl_pengguna set pengguna_nama='$nama',pengguna_jenkel='$jenkel',pengguna_username='$username',pengguna_email='$email',pengguna_nohp='$nohp',pengguna_level='$level',pengguna_photo='$gambar' where pengguna_id='$kode'");
		return $hsl;
	}
	function update_pengguna($kode,$nama,$jenkel,$username,$password,$email,$nohp,$level,$gambar){
		$hsl=$this->db->query("UPDATE tbl_pengguna set pengguna_nama='$nama',pengguna_jenkel='$jenkel',pengguna_username='$username',pengguna_password='$password',pengguna_email='$email',pengguna_nohp='$nohp',pengguna_level='$level',pengguna_photo='$gambar' where pengguna_id='$kode'");
		return $hsl;
	}

	function update_pengguna_tanpa_pass_dan_gambar($kode,$nama,$jenkel,$username,$password,$email,$nohp,$level){
		$hsl=$this->db->query("UPDATE tbl_pengguna set pengguna_nama='$nama',pengguna_jenkel='$jenkel',pengguna_username='$username',pengguna_email='$email',pengguna_nohp='$nohp',pengguna_level='$level' where pengguna_id='$kode'");
		return $hsl;
	}
	function update_pengguna_tanpa_gambar($kode,$nama,$jenkel,$username,$password,$email,$nohp,$level){
		$hsl=$this->db->query("UPDATE tbl_pengguna set pengguna_nama='$nama',pengguna_jenkel='$jenkel',pengguna_username='$username',pengguna_password='$password',pengguna_email='$email',pengguna_nohp='$nohp',pengguna_level='$level' where pengguna_id='$kode'");
		return $hsl;
	}
	//END UPDATE PENGGUNA//

	function hapus_pengguna($kode){
		$hsl=$this->db->query("DELETE FROM tbl_pengguna where pengguna_id='$kode'");
		return $hsl;
	}
	function getusername($id){
		$hsl=$this->db->query("SELECT * FROM tbl_pengguna where pengguna_id='$id'");
		return $hsl;
	}
	function resetpass($id,$pass){
		$hsl=$this->db->query("UPDATE tbl_pengguna set pengguna_password=md5('$pass') where pengguna_id='$id'");
		return $hsl;
	}

	function get_pengguna_login($kode){
		$hsl=$this->db->query("SELECT * FROM tbl_pengguna where pengguna_id='$kode'");
		return $hsl;
	}


}