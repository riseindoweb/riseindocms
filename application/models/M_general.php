<?php
class M_general extends CI_Model{

	function provinces_filter(){
		$this->db->select('*');
	    $this->db->from('provinces'); 
	    $this->db->order_by('id','asc');         
	    $query = $this->db->get(); 
        return $query->result_array();
	}
	function regencies_filter($id){
		$this->db->select('*');
	    $this->db->from('regencies'); 
	    $this->db->where('province_id',$id);
	    $this->db->order_by('id','asc');         
	    $query = $this->db->get(); 
        return $query->result_array();
	}
	function get_all_risk(){
		$this->db->select('*');
	    $this->db->from('category_resiko');        
	    $query = $this->db->get(); 
        return $query->result_array();
	}
	function get_all_skema(){
		$this->db->select('*');
	    $this->db->from('skema');        
	    $query = $this->db->get(); 
        return $query->result_array();
	}
	function proyek_permodalan_filter(){
		$hsl=$this->db->query("SELECT `id_permintaan_modal`, `id_petani`, `title`, p.nama as nama_petani,  r.nama as nama_resiko, s.nama as nama_skema, `start_from`, `profit`, `lama_proyek`, l.name as nama_lokasi, `id_lokasi`, pm.`id_category_resiko`, pm.`id_skema`, `kebutuhan`, pm.`status`, pi.url_images as url_images, pm.`date_add`, `doc_perusahaan` FROM  permintaan_modal pm
			left JOIN post_image pi ON pm.id_permintaan_modal = pi.id_post
			LEFT JOIN petani p ON p.petani_id = pm.id_petani
			LEFT JOIN skema s ON s.id_skema = pm.id_skema
			LEFT JOIN category_resiko r ON r.id_category_resiko = pm.id_category_resiko
			LEFT JOIN regencies l ON l.id = pm.id_lokasi
			GROUP BY pi.id_post");
        $query = $hsl->result_array();
        return array( 'proyek_permodalan_list' => $query, 'resultCode' => 'OK');
	}
	function proyek_permodalan_view($id){
		$hsl=$this->db->query("SELECT `id_permintaan_modal`, `id_petani`, `title`, p.nama as nama_petani,  r.nama as nama_resiko, s.nama as nama_skema, `start_from`, `profit`, `lama_proyek`, l.name as nama_lokasi, `id_lokasi`, pm.`id_category_resiko`, pm.`id_skema`, `kebutuhan`, pm.`status`, pm.`date_add`, `doc_perusahaan` FROM  permintaan_modal pm
			-- left JOIN post_image pi ON pm.id_permintaan_modal = pi.id_post
			LEFT JOIN petani p ON p.petani_id = pm.id_petani
			LEFT JOIN skema s ON s.id_skema = pm.id_skema
			LEFT JOIN category_resiko r ON r.id_category_resiko = pm.id_category_resiko
			LEFT JOIN regencies l ON l.id = pm.id_lokasi
			where pm.id_permintaan_modal = '$id'");
        $query = $hsl->row_array();
		$this->db->select('*');
	    $this->db->from('post_image');        
	    $this->db->where('id_post', $id);        
	    $querys = $this->db->get(); 
        $row = $querys->result_array();
		$MyObjects = array();
		$MyObjects['imageslist'] = $row;
        return array( 'proyek_permodalan' => array_merge($query,$MyObjects), 'resultCode' => 'OK');
	}
	function sosmed_filter(){
		$this->db->select('*');
	    $this->db->from('sosmed');        
	    $query = $this->db->get(); 
        $row = $query->result_array();
        return array('sosmed_list' => $row, 'resultCode' => 'OK');
	}
	function simpan_agenda($nama_agenda,$deskripsi,$mulai,$selesai,$tempat,$waktu,$keterangan){
		$author=$this->session->userdata('nama');
		$hsl=$this->db->query("INSERT INTO tbl_agenda(agenda_nama,agenda_deskripsi,agenda_mulai,agenda_selesai,agenda_tempat,agenda_waktu,agenda_keterangan,agenda_author) VALUES ('$nama_agenda','$deskripsi','$mulai','$selesai','$tempat','$waktu','$keterangan','$author')");
		return $hsl;
	}
	function update_agenda($kode,$nama_agenda,$deskripsi,$mulai,$selesai,$tempat,$waktu,$keterangan){
		$author=$this->session->userdata('nama');
		$hsl=$this->db->query("UPDATE tbl_agenda SET agenda_nama='$nama_agenda',agenda_deskripsi='$deskripsi',agenda_mulai='$mulai',agenda_selesai='$selesai',agenda_tempat='$tempat',agenda_waktu='$waktu',agenda_keterangan='$keterangan',agenda_author='$author' where agenda_id='$kode'");
		return $hsl;
	}
	function hapus_agenda($kode){
		$hsl=$this->db->query("DELETE FROM tbl_agenda WHERE agenda_id='$kode'");
		return $hsl;
	}

	//front-end
	function get_agenda_home(){
		$hsl=$this->db->query("SELECT tbl_agenda.*,DATE_FORMAT(agenda_tanggal,'%d/%m/%Y') AS tanggal FROM tbl_agenda ORDER BY agenda_id DESC limit 3");
		return $hsl;
	}
	function agenda(){
		$hsl=$this->db->query("SELECT tbl_agenda.*,DATE_FORMAT(agenda_tanggal,'%d/%m/%Y') AS tanggal FROM tbl_agenda ORDER BY agenda_id DESC");
		return $hsl;
	}
	function agenda_perpage($offset,$limit){
		$hsl=$this->db->query("SELECT tbl_agenda.*,DATE_FORMAT(agenda_tanggal,'%d/%m/%Y') AS tanggal FROM tbl_agenda ORDER BY agenda_id DESC limit $offset,$limit");
		return $hsl;
	}


} 